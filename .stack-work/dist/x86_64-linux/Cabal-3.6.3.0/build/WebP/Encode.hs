-- GENERATED by C->Haskell Compiler, version 0.28.8 Switcheroo, 25 November 2017 (Haskell)
-- Edit the ORIGNAL .chs file instead!


{-# LINE 1 "src/WebP/Encode.chs" #-}
module WebP.Encode ( webPEncodeRGB
                   , webPEncodeRGBA
                   , webPEncodeLosslessRGB
                   , webPEncodeLosslessRGBA
                   ) where
import qualified Foreign.C.Types as C2HSImp
import qualified Foreign.Ptr as C2HSImp



import Data.Coerce (coerce)
import Foreign.C.Types (CFloat, CInt, CSize)
import Foreign.Marshal.Alloc (alloca)
import Foreign.Ptr (Ptr)
import Foreign.Storable (peek)



type UInt8 = (C2HSImp.CUChar)
{-# LINE 15 "src/WebP/Encode.chs" #-}


webPEncodeRGB :: (Ptr UInt8) -> (CInt) -> (CInt) -> (CInt) -> (CFloat) -> IO ((CSize), (Ptr UInt8))
webPEncodeRGB a1 a2 a3 a4 a5 =
  let {a1' = id a1} in 
  let {a2' = fromIntegral a2} in 
  let {a3' = fromIntegral a3} in 
  let {a4' = fromIntegral a4} in 
  let {a5' = realToFrac a5} in 
  alloca $ \a6' -> 
  webPEncodeRGB'_ a1' a2' a3' a4' a5' a6' >>= \res ->
  let {res' = coerce res} in
  peek  a6'>>= \a6'' -> 
  return (res', a6'')

{-# LINE 17 "src/WebP/Encode.chs" #-}

webPEncodeRGBA :: (Ptr UInt8) -> (CInt) -> (CInt) -> (CInt) -> (CFloat) -> IO ((CSize), (Ptr UInt8))
webPEncodeRGBA a1 a2 a3 a4 a5 =
  let {a1' = id a1} in 
  let {a2' = fromIntegral a2} in 
  let {a3' = fromIntegral a3} in 
  let {a4' = fromIntegral a4} in 
  let {a5' = realToFrac a5} in 
  alloca $ \a6' -> 
  webPEncodeRGBA'_ a1' a2' a3' a4' a5' a6' >>= \res ->
  let {res' = coerce res} in
  peek  a6'>>= \a6'' -> 
  return (res', a6'')

{-# LINE 18 "src/WebP/Encode.chs" #-}

webPEncodeLosslessRGB :: (Ptr UInt8) -> (CInt) -> (CInt) -> (CInt) -> IO ((CSize), (Ptr UInt8))
webPEncodeLosslessRGB a1 a2 a3 a4 =
  let {a1' = id a1} in 
  let {a2' = fromIntegral a2} in 
  let {a3' = fromIntegral a3} in 
  let {a4' = fromIntegral a4} in 
  alloca $ \a5' -> 
  webPEncodeLosslessRGB'_ a1' a2' a3' a4' a5' >>= \res ->
  let {res' = coerce res} in
  peek  a5'>>= \a5'' -> 
  return (res', a5'')

{-# LINE 19 "src/WebP/Encode.chs" #-}

webPEncodeLosslessRGBA :: (Ptr UInt8) -> (CInt) -> (CInt) -> (CInt) -> IO ((CSize), (Ptr UInt8))
webPEncodeLosslessRGBA a1 a2 a3 a4 =
  let {a1' = id a1} in 
  let {a2' = fromIntegral a2} in 
  let {a3' = fromIntegral a3} in 
  let {a4' = fromIntegral a4} in 
  alloca $ \a5' -> 
  webPEncodeLosslessRGBA'_ a1' a2' a3' a4' a5' >>= \res ->
  let {res' = coerce res} in
  peek  a5'>>= \a5'' -> 
  return (res', a5'')

{-# LINE 20 "src/WebP/Encode.chs" #-}


foreign import ccall safe "WebP/Encode.chs.h WebPEncodeRGB"
  webPEncodeRGB'_ :: ((C2HSImp.Ptr C2HSImp.CUChar) -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CFloat -> ((C2HSImp.Ptr (C2HSImp.Ptr C2HSImp.CUChar)) -> (IO C2HSImp.CULong)))))))

foreign import ccall safe "WebP/Encode.chs.h WebPEncodeRGBA"
  webPEncodeRGBA'_ :: ((C2HSImp.Ptr C2HSImp.CUChar) -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CFloat -> ((C2HSImp.Ptr (C2HSImp.Ptr C2HSImp.CUChar)) -> (IO C2HSImp.CULong)))))))

foreign import ccall safe "WebP/Encode.chs.h WebPEncodeLosslessRGB"
  webPEncodeLosslessRGB'_ :: ((C2HSImp.Ptr C2HSImp.CUChar) -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CInt -> ((C2HSImp.Ptr (C2HSImp.Ptr C2HSImp.CUChar)) -> (IO C2HSImp.CULong))))))

foreign import ccall safe "WebP/Encode.chs.h WebPEncodeLosslessRGBA"
  webPEncodeLosslessRGBA'_ :: ((C2HSImp.Ptr C2HSImp.CUChar) -> (C2HSImp.CInt -> (C2HSImp.CInt -> (C2HSImp.CInt -> ((C2HSImp.Ptr (C2HSImp.Ptr C2HSImp.CUChar)) -> (IO C2HSImp.CULong))))))
