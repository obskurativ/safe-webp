{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -w #-}
module Paths_webp (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where


import qualified Control.Exception as Exception
import qualified Data.List as List
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude


#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,2] []

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir `joinFileName` name)

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath




bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath
bindir     = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/bin"
libdir     = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/lib/x86_64-linux-ghc-9.2.7/webp-0.1.0.2-2Df3n9cRfeMIneBSyfLdfd"
dynlibdir  = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/lib/x86_64-linux-ghc-9.2.7"
datadir    = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/share/x86_64-linux-ghc-9.2.7/webp-0.1.0.2"
libexecdir = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/libexec/x86_64-linux-ghc-9.2.7/webp-0.1.0.2"
sysconfdir = "/home/gard/obskurativ/servil/.stack-work/install/x86_64-linux/21d62e152d12fc314f3c1cc9a3938652a25dd47429373c7eeaa8dc42016a0787/9.2.7/etc"

getBinDir     = catchIO (getEnv "webp_bindir")     (\_ -> return bindir)
getLibDir     = catchIO (getEnv "webp_libdir")     (\_ -> return libdir)
getDynLibDir  = catchIO (getEnv "webp_dynlibdir")  (\_ -> return dynlibdir)
getDataDir    = catchIO (getEnv "webp_datadir")    (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "webp_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "webp_sysconfdir") (\_ -> return sysconfdir)



joinFileName :: String -> String -> FilePath
joinFileName ""  fname = fname
joinFileName "." fname = fname
joinFileName dir ""    = dir
joinFileName dir fname
  | isPathSeparator (List.last dir) = dir ++ fname
  | otherwise                       = dir ++ pathSeparator : fname

pathSeparator :: Char
pathSeparator = '/'

isPathSeparator :: Char -> Bool
isPathSeparator c = c == '/'
